import dotenv from 'dotenv'
dotenv.config()

import mysql from 'mysql2'

const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    port: Number(process.env.PORT),
    user: process.env.DB_USER,
    password: process.env.BD_PASSWORD,
    database: process.env.DB_NAME
})

export default connection