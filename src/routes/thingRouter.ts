import express, {Request, Response} from 'express'
import Thing from '../types/thing'
import * as modelManager from '../models/thing'

const routerManager = express.Router()

routerManager.get('/', async (request:Request, response:Response) => {
    modelManager.findAllItem((error:Error, result:Thing[]) => {
        if(error) {
            return response.status(500).json({"errorMessage": error.message})
        }
        response.status(200).json(result)
    })
})

routerManager.post('/', async (request:Request, response:Response) => {
    const name = request.body.name
    modelManager.createItem(name, (error:Error, id:number) => {
        if(error) {
            return response.status(500).json({"errorMessage": error.message})
        }
        response.status(201).json(id)
    })    
})

routerManager.get('/:id', async (request:Request, response:Response) => {
    const id:number = Number(request.params.id)
    modelManager.findOneItem(id, (error:Error, result:Thing) => {
        if(error) {
            return response.status(500).json({"errorMessage": error.message})
        }
        response.status(200).json({data:result})
    })
})


routerManager.put('/:id', async (request:Request, response:Response) => {
    const body:Thing = request.body
    modelManager.updateItem(body, (error:Error) => {
        if(error) {
            return response.status(500).json({"errorMessage": error.message})
        }
        response.status(200).send()
    })    
})

routerManager.delete('/:id',async (request:Request, response:Response) => {
    const id:number = Number(request.params.id)
    modelManager.deleteItem(id, (error:Error) => {
        if(error) {
            return response.status(500).json({"errorMessage": error.message})
        }
        response.status(200).send()
    })
})



export default routerManager