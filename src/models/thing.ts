import Thing from "../types/thing"
import connection from "../db-config"
import { OkPacket, RowDataPacket } from "mysql2"
import { error } from "console"

export const createItem = (name: string, callback: Function) => {
    const queryString  = "INSERT INTO thing (name) VALUES (?)"
    connection.query(queryString, [name], (error, result) => {
        if(error) {
            callback(error)
        }
        const insetId = (<OkPacket> result).insertId
        callback(null, insetId)
    })
}

export const findOneItem = (id: number, callback: Function) => {
    const queryString = "SELECT * FROM thing WHERE id = ?"
    connection.query(queryString, [id], (error, result) => {
        if(error) {
            callback(error)
        }

        const row = (<RowDataPacket> result)[0]
        const thing: Thing = {
            id: row.id,
            name: row.name
        }
        callback(null, thing)
    })
}

export const findAllItem = (callback: Function) => {
    const queryString = "SELECT * FROM thing"
    connection.query(queryString, (error, result) => {
        if(error) {
            callback(error)
        }

        const rows = (<RowDataPacket[]> result)
        const things: Thing[] = []

        rows.forEach(row => {
            const thing: Thing = {
                id: row.id,
                name: row.name
            }
            things.push(thing)
        })

        callback(null, things)
    })
}

export const updateItem = (data: Thing, callback: Function) => {
    const queryString = "UPDATE thing SET name = ? WHERE id = ?"
    connection.query(queryString, [data.name, data.id], (error) => {
        if(error) {
            callback(error)
        }
        callback(null)
    })
}

export const deleteItem = (id: number, callback: Function) => {
    const queryString = "DELETE FROM thing WHERE id = ?"
    connection.query(queryString, [id], (error) => {
        if(error) {
            callback(error)
        }
        callback(null)
    })
}