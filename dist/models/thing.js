"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteItem = exports.updateItem = exports.findAllItem = exports.findOneItem = exports.createItem = void 0;
const db_config_1 = __importDefault(require("../db-config"));
const createItem = (name, callback) => {
    const queryString = "INSERT INTO thing (name) VALUES (?)";
    db_config_1.default.query(queryString, [name], (error, result) => {
        if (error) {
            callback(error);
        }
        const insetId = result.insertId;
        callback(null, insetId);
    });
};
exports.createItem = createItem;
const findOneItem = (id, callback) => {
    const queryString = "SELECT * FROM thing WHERE id = ?";
    db_config_1.default.query(queryString, [id], (error, result) => {
        if (error) {
            callback(error);
        }
        const row = result[0];
        const thing = {
            id: row.id,
            name: row.name
        };
        callback(null, thing);
    });
};
exports.findOneItem = findOneItem;
const findAllItem = (callback) => {
    const queryString = "SELECT * FROM thing";
    db_config_1.default.query(queryString, (error, result) => {
        if (error) {
            callback(error);
        }
        const rows = result;
        const things = [];
        rows.forEach(row => {
            const thing = {
                id: row.id,
                name: row.name
            };
            things.push(thing);
        });
        callback(null, things);
    });
};
exports.findAllItem = findAllItem;
const updateItem = (data, callback) => {
    const queryString = "UPDATE thing SET name = ? WHERE id = ?";
    db_config_1.default.query(queryString, [data.name, data.id], (error) => {
        if (error) {
            callback(error);
        }
        callback(null);
    });
};
exports.updateItem = updateItem;
const deleteItem = (id, callback) => {
    const queryString = "DELETE FROM thing WHERE id = ?";
    db_config_1.default.query(queryString, [id], (error) => {
        if (error) {
            callback(error);
        }
        callback(null);
    });
};
exports.deleteItem = deleteItem;
