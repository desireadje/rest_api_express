"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const modelManager = __importStar(require("../models/thing"));
const routerManager = express_1.default.Router();
routerManager.get('/', async (request, response) => {
    modelManager.findAllItem((error, result) => {
        if (error) {
            return response.status(500).json({ "errorMessage": error.message });
        }
        response.status(200).json(result);
    });
});
routerManager.post('/', async (request, response) => {
    const name = request.body.name;
    modelManager.createItem(name, (error, id) => {
        if (error) {
            return response.status(500).json({ "errorMessage": error.message });
        }
        response.status(201).json(id);
    });
});
routerManager.get('/:id', async (request, response) => {
    const id = Number(request.params.id);
    modelManager.findOneItem(id, (error, result) => {
        if (error) {
            return response.status(500).json({ "errorMessage": error.message });
        }
        response.status(200).json({ data: result });
    });
});
routerManager.put('/:id', async (request, response) => {
    const body = request.body;
    modelManager.updateItem(body, (error) => {
        if (error) {
            return response.status(500).json({ "errorMessage": error.message });
        }
        response.status(200).send();
    });
});
routerManager.delete('/:id', async (request, response) => {
    const id = Number(request.params.id);
    modelManager.deleteItem(id, (error) => {
        if (error) {
            return response.status(500).json({ "errorMessage": error.message });
        }
        response.status(200).send();
    });
});
exports.default = routerManager;
